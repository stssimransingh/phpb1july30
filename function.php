<?php 
class calc{
    private $a = 0;
    private $b = 0;
    function __construct($num1 , $num2){
        $this->a = $num1;
        $this->b = $num2;
    }
    function add(){
        return $this->a + $this->b;
    }
    function __destruct(){
        $this->a = 0;
        $this->b = 0;
    }
}
?>