<?php 
class a{
    protected $a = 10;
}
class b extends a{
    public $b = 20;

    protected function abcd(){
        return 10;
    }
    function call(){
        return $this->a;
    }
}
class c extends b{
    private $c = 30;
    function add(){
        return $this->a + $this->c + $this->abcd();
    }
}
$obj = new b();
echo $obj->a;
?>